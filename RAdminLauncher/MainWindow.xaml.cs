﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Autofac;
using RAdminLauncherCore.Iterfaces;
using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncher
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IRemoteAdminLaunchContoller _controller;

        public MainWindow()
        {
            InitializeComponent();
            _controller = App.IoC.Resolve<IRemoteAdminLaunchContoller>();
        }

        private void RefreshDataGrid(IReadOnlyCollection<IEndPoint> colection)
        {
            dgMain.ItemsSource = colection;
            dgMain.Items.Refresh();
        }

        private void RefreshDataGrid()
        {
            dgMain.ItemsSource = _controller.EndPointsCollection.Endpoints;
            dgMain.Items.Refresh();
        }

        private void OnSaveChanges()
        {
            try
            {
                RefreshDataGrid();
                _controller.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Не удалось сохранить изменения в файл. {exception.Message}");
            }
        }
        
        private void FMain_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _controller.LoadData();
                RefreshDataGrid();
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Не удалось загрузить данные. {exception.Message}");
            }
        }

        private void dgMain_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgMain.SelectedItem is IEndPoint selectedItem)
            {
                try
                {
                    _controller.RemoteAdminLauncher.StartRAdmin(selectedItem);
                }
                catch (Exception exception)
                {
                    MessageBox.Show($"Не удалось запустить RAdmin. {exception.Message}");
                }
            }
            else
            {
                MessageBox.Show("Выберите контакт для запуска");
            }
        }

        private void tbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var filteredEndpoints = _controller.EndPointsCollection.GetEndpointsByFilter(tbName.Text, tbIpAddress.Text, tbAddress.Text);
                RefreshDataGrid(filteredEndpoints);
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Ошибка при фильтрации. {exception.Message}");
            }
        }


        private void bEdit_Click(object sender, RoutedEventArgs e)
        {
            if (dgMain.SelectedItem is IEndPoint selectedItem)
            {
                try
                {
                    var window = new WindowAddEdit(selectedItem);
                    window.FAddEdit.ShowDialog();
                    OnSaveChanges();
                }
                catch (Exception exception)
                {
                    MessageBox.Show($"Произошла ошибка при изменении точки. {exception.Message}");
                }
            }
            else
            {
                MessageBox.Show("Выберите контакт для редактирования!");
            }
        }

        private void bAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var window = new WindowAddEdit();
                window.FAddEdit.ShowDialog();
                OnSaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Произошла ошибка при добавлении точки. {exception.Message}");
            }
        }

        private void bDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dgMain.SelectedItem is IEndPoint selectedItem)
            {
                try
                {
                    var id = selectedItem.Id;
                    _controller.EndPointsCollection.RemoveEndpoint(id);
                    OnSaveChanges();
                    MessageBox.Show("Контакт удален!");
                }
                catch (Exception exception)
                {
                    MessageBox.Show($"Ошибка при удалении точки. {exception.Message}");
                }
               
            }
            else
            {
                MessageBox.Show("Выберите контакт для редактирования!");
            }
        }
    }
}