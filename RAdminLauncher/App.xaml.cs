﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Autofac;
using RAdminLauncherCore.Implementations;
using RAdminLauncherCore.Implementations.Configuration;
using RAdminLauncherCore.Implementations.DataSource;
using RAdminLauncherCore.Implementations.Encryptors.Rfc2898;
using RAdminLauncherCore.Implementations.Process;
using RAdminLauncherCore.Iterfaces;
using RAdminLauncherCore.Iterfaces.Configuration;
using RAdminLauncherCore.Iterfaces.DataSource;
using RAdminLauncherCore.Iterfaces.Encryptors;
using RAdminLauncherCore.Iterfaces.Process;
using RAdminLauncherCore.Serializers;

namespace RAdminLauncher
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static IContainer IoC { get; private set; }

        public App()
        {
            
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var builder = new ContainerBuilder();
            builder.RegisterType<EndPointCollectionDataSerializer>();
            builder.RegisterType<EndPointListDataSerializer>();
            builder.RegisterType<RemoteAdminProcess>().As<IRemoteAdminProcess>().SingleInstance();
            builder.RegisterType<Rfc2898Encryptor>().As<IEncryptor>().SingleInstance();
            builder.RegisterType<Configuration>().As<IConfiguration>().SingleInstance();
            builder.RegisterType<JsonDataSourceV1>().As<IDataSource>().SingleInstance();
            builder.RegisterType<RemoteAdminLaunchController>().As<IRemoteAdminLaunchContoller>().SingleInstance();
            var container = builder.Build();
            IoC = container;
        }
    }
}
