﻿using System;
using System.Windows;
using Autofac;
using FluentValidation;
using RAdminLauncherCore.Enums;
using RAdminLauncherCore.Exceptions;
using RAdminLauncherCore.Implementations.Dtos;
using RAdminLauncherCore.Implementations.EndPoint;
using RAdminLauncherCore.Iterfaces;
using RAdminLauncherCore.Iterfaces.EndPoint;
using RAdminLauncherCore.Validators;
using RAdminLauncherCore.Extensions;
using RAdminLauncherCore.Iterfaces.Configuration;

namespace RAdminLauncher
{
    /// <summary>
    /// Логика взаимодействия для WindowAddEdit.xaml
    /// </summary>
    public partial class WindowAddEdit : Window
    {
        private IEndPointDto _processedItem;
        private IRemoteAdminLaunchContoller _controller;
        private IConfiguration _configuration;

        public WindowAddEdit()
        {
            InitializeComponent();
            InitializeData(new EndPointDto());
        }

        public WindowAddEdit(IEndPoint point)
        {
            InitializeComponent();
            InitializeData(new EndPointDto(point));
        }

        private void InitializeData(IEndPointDto endpointDto)
        {
            _controller = App.IoC.Resolve<IRemoteAdminLaunchContoller>();
            _configuration = App.IoC.Resolve<IConfiguration>();
            var port = endpointDto.Port.IsNullOrEmpty() ? _configuration.DefaultPort : endpointDto.Port;
            endpointDto.Port = port;
            _processedItem = endpointDto;
            MapDtoToView();
        }

        private void MapDtoToView()
        {
            tbIpAddress.Text = _processedItem.Ip;
            tbAddress.Text = _processedItem.Address;
            tbName.Text = _processedItem.Name;
            tbPassword.Password = _processedItem.Password;
            tbPort.Text = _processedItem.Port;
            tbUser.Text = _processedItem.User;
            cbVersion.Text = ParseVersionToString(_processedItem.Version);
        }

        private bool MapViewToDto()
        {
            _processedItem.Ip = tbIpAddress.Text;
            _processedItem.Address = tbAddress.Text;
            _processedItem.Name = tbName.Text;
            _processedItem.Password = tbPassword.Password;
            _processedItem.Port = tbPort.Text;
            _processedItem.User = tbUser.Text;
            _processedItem.Version = ParseVersionFromString(cbVersion.Text);

            var validator = new EndPointDtoValidator();
            try
            {
                validator.ValidateAndThrow(_processedItem);
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Введены некорректные данные. {exception.Message}");
                return false;
            }

            return true;
        }

        private void bAddEdit_Click(object sender, RoutedEventArgs e)
        {
            var isSuccess = MapViewToDto();
            if (!isSuccess)
            {
                return;
            }
            var endPoint = new EndPoint(_processedItem);
            try
            {
                _controller.EndPointsCollection.AddOrUpdateEndpoint(endPoint);
                MessageBox.Show("Контакт сохранен");
                Close();
            }
            catch (EndPointAlreadyExistsException exception)
            {
                MessageBox.Show($"Конфликт ip адресов. {exception.Message}");
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Ошибка при сохранении точки. {exception.Message}");
            }
        }

        private EndPointVersion ParseVersionFromString(string value)
        {
            if (Enum.TryParse<EndPointVersion>(value, out var version))
            {
                return version;
            }
            else
            {
                return (EndPointVersion)(-1);
            }
        }

        private string ParseVersionToString(EndPointVersion value)
        {
            return value.ToString();
        }
    }
}
