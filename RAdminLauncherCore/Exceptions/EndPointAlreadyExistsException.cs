﻿using System;
using System.Net;

namespace RAdminLauncherCore.Exceptions
{
    public class EndPointAlreadyExistsException : Exception
    {
        public EndPointAlreadyExistsException(IPAddress ipAddress) : base($"EndPoint с указанным IP ('{ipAddress}') уже добавлен")
        {

        }

        public EndPointAlreadyExistsException(IPAddress ipAddress, Exception exception) : base($"EndPoint с указанным IP ('{ipAddress}') уже добавлен", exception)
        {

        }
    }
}
