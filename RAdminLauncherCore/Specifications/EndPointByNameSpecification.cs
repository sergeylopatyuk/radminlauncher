﻿using System;
using NSpecifications;
using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Specifications
{
    public class EndPointByNameSpecification : Spec<IEndPoint>
    {
        public EndPointByNameSpecification(string filter) : base(x=>x.Name.IndexOf(filter, StringComparison.OrdinalIgnoreCase) > -1)
        {
        }
    }
}
