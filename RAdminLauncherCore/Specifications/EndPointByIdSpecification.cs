﻿using NSpecifications;
using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Specifications
{
    public class EndPointByIdSpecification : Spec<IEndPoint>
    {
        public EndPointByIdSpecification(string id) : base(x => x.Id == id)
        {
        }
    }
}
