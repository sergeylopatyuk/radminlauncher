﻿using System;
using NSpecifications;
using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Specifications
{
    public class EndPointByAddressSpecification : Spec<IEndPoint>
    {
        public EndPointByAddressSpecification(string filter) : base(x => x.Address.IndexOf(filter, StringComparison.OrdinalIgnoreCase) > -1)
        {
        }
    }
}
