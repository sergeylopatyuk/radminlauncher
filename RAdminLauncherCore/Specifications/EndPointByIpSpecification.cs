﻿using System;
using System.Net;
using NSpecifications;
using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Specifications
{
    public class EndPointByIpSpecification : Spec<IEndPoint>
    {
        public EndPointByIpSpecification(string filter) : base(x=>x.Ip.ToString().IndexOf(filter, StringComparison.OrdinalIgnoreCase) > -1)
        {
        }

        public EndPointByIpSpecification(IPAddress filter) : base(x => x.Ip.Equals(filter))
        {
        }
    }
}
