﻿using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Extensions
{
    public static class EndPointExtension
    {
        public static bool IsSameIp(this IEndPoint source, IEndPoint target)
        {
            return source.Ip.Equals(target.Ip);
        }
    }
}
