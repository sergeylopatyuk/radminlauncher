﻿using Newtonsoft.Json;
using RAdminLauncherCore.Iterfaces.Serializers;

namespace RAdminLauncherCore.Serializers
{
    public class DataSerializer<T> : IDataSerializer<T>
    {
        private Formatting GetFormattingSettings()
        {
            return Formatting.Indented;
        }

        private JsonSerializerSettings GetSerializatorSettings()
        {
            return new JsonSerializerSettings()
            {
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                TypeNameHandling = TypeNameHandling.All,
                ContractResolver = new PrivateSetterContractResolver()
            };
        }

        public T Desserialize(string data)
        {
            var result = JsonConvert.DeserializeObject<T>(data, GetSerializatorSettings());
            return result;
        }

        public string Serialize(T endpointCollection)
        {
            var serializedData = JsonConvert.SerializeObject(endpointCollection, GetFormattingSettings(), GetSerializatorSettings());
            return serializedData;
        }
    }
}
