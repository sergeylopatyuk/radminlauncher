﻿using System.Configuration;
using RAdminLauncherCore.Iterfaces.Configuration;

namespace RAdminLauncherCore.Implementations.Configuration
{
    public class Configuration : IConfiguration
    {
        public string RemoteAdminFilePath { get; }
        public int RemoteAdminSleepInterval { get; }
        public string JsonPath { get; }
        public string EncryptionKey { get; }
        public string DefaultPort { get; }

        public Configuration()
        {
            RemoteAdminFilePath = ConfigurationManager.AppSettings["RAdminPath"];
            RemoteAdminSleepInterval = int.Parse(ConfigurationManager.AppSettings["RAdminSleepEntry"]);
            JsonPath = ConfigurationManager.AppSettings["JsonPath"];
            EncryptionKey = ConfigurationManager.AppSettings["EncryptionKey"];
            DefaultPort = ConfigurationManager.AppSettings["DefaultPort"];
        }
    }
}
