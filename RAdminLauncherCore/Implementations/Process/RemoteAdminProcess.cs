﻿using System;
using System.Threading;
using WindowsInput;
using WindowsInput.Native;
using RAdminLauncherCore.Enums;
using RAdminLauncherCore.Iterfaces.Configuration;
using RAdminLauncherCore.Iterfaces.EndPoint;
using RAdminLauncherCore.Iterfaces.Process;


namespace RAdminLauncherCore.Implementations.Process
{
    public class RemoteAdminProcess : IRemoteAdminProcess
    {
        private readonly IConfiguration _configuration;

        public RemoteAdminProcess(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private string GetConnectCommand(IEndPoint endPoint)
        {
            return @"/connect:" + endPoint.Ip + ":" + endPoint.Port;
        }

        public void StartRAdmin(IEndPoint endPoint)
        {
            if(endPoint == null)
                throw new ArgumentException("Конечная точка не может быть пустой");

            var command = GetConnectCommand(endPoint);
            var process = System.Diagnostics.Process.Start(_configuration.RemoteAdminFilePath, command);
            Thread.Sleep(_configuration.RemoteAdminSleepInterval);

            var simulator = new InputSimulator();
            if (endPoint.Version == EndPointVersion.V3)
            {
                simulator.Keyboard.TextEntry(endPoint.User);
                simulator.Keyboard.KeyPress(VirtualKeyCode.TAB);
                simulator.Keyboard.TextEntry(endPoint.Password);
                simulator.Keyboard.KeyPress(VirtualKeyCode.EXECUTE);
            }
            else if (endPoint.Version == EndPointVersion.V2)
            {
                simulator.Keyboard.TextEntry(endPoint.Password);
                simulator.Keyboard.KeyPress(VirtualKeyCode.EXECUTE);
            }
        }
    }
}
