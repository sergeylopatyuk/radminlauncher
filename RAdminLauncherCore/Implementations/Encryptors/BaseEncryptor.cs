﻿using RAdminLauncherCore.Iterfaces.Configuration;
using RAdminLauncherCore.Iterfaces.Encryptors;

namespace RAdminLauncherCore.Implementations.Encryptors
{
    public abstract class BaseEncryptor : IEncryptor
    {
        protected IConfiguration Configurations { get; }

        protected BaseEncryptor(IConfiguration configuration)
        {
            Configurations = configuration;
        }

        public abstract string Encrypt(string data);
        public abstract string Decrypt(string data);
    }
}
