﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using RAdminLauncherCore.Exceptions;
using RAdminLauncherCore.Extensions;
using RAdminLauncherCore.Iterfaces.EndPoint;
using RAdminLauncherCore.Iterfaces.EndPointCollection;
using RAdminLauncherCore.Specifications;

namespace RAdminLauncherCore.Implementations.EndPointCollection
{
    public class EndPointCollection : IEndPointsCollection
    {
        private readonly List<IEndPoint> _endPoints;
        public IReadOnlyCollection<IEndPoint> Endpoints => _endPoints;
       
        internal EndPointCollection()
        {
            _endPoints = new List<IEndPoint>();
        }

        internal EndPointCollection(IEnumerable<IEndPoint> endPoints)
        {
            _endPoints = new List<IEndPoint>();
            _endPoints.AddRange(endPoints);
        }

        public IReadOnlyCollection<IEndPoint> GetEndpointsByFilter(string name, string ip, string address)
        {
            var filterSpecification = new EndPointByAddressSpecification(address) &
                                      new EndPointByIpSpecification(ip) & 
                                      new EndPointByNameSpecification(name);

            return _endPoints.Where(filterSpecification).ToList();
        }

        public void AddOrUpdateEndpoint(IEndPoint endPoint)
        {
            var alreadyExistsEndpoint = _endPoints.FirstOrDefault(new EndPointByIdSpecification(endPoint.Id));
            var endPointIsAlreadyExists = alreadyExistsEndpoint != null;
            var ipAddressAlreadyExists = IsAlradyExists(endPoint.Ip);
            var ipAddressIsDublicated = false;
           
            if (endPointIsAlreadyExists)
            {
                ipAddressIsDublicated = !alreadyExistsEndpoint.IsSameIp(endPoint) && ipAddressAlreadyExists;
            }
            else
            {
                ipAddressIsDublicated = ipAddressAlreadyExists;
            }

            if (ipAddressIsDublicated)
            {
                throw new EndPointAlreadyExistsException(endPoint.Ip);
            }

            var index = _endPoints.IndexOf(alreadyExistsEndpoint);
            if (index > -1)
            {
                _endPoints[index] = endPoint;
            }
            else
            {
                _endPoints.Add(endPoint);
            }
        }

        public int RemoveEndpoint(string id)
        {
            return _endPoints.RemoveAll(x => x.Id == id);
        }

        private bool IsAlradyExists(IPAddress ipAddress)
        {
            return _endPoints.FirstOrDefault(new EndPointByIpSpecification(ipAddress)) != null;
        }
    }
}
