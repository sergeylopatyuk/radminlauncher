﻿using System;
using RAdminLauncherCore.Iterfaces;
using RAdminLauncherCore.Iterfaces.DataSource;
using RAdminLauncherCore.Iterfaces.EndPointCollection;
using RAdminLauncherCore.Iterfaces.Process;

namespace RAdminLauncherCore.Implementations
{
    public class RemoteAdminLaunchController : IRemoteAdminLaunchContoller
    {
        private readonly IDataSource _dataSource;
        public IRemoteAdminProcess RemoteAdminLauncher { get; }
        public IEndPointsCollection EndPointsCollection { get; private set; }
        
        public RemoteAdminLaunchController(IDataSource dataSource, IRemoteAdminProcess processController)
        {
            _dataSource = dataSource;
            RemoteAdminLauncher = processController;
        }

        public void SaveChanges()
        {
            _dataSource?.SaveChanges(EndPointsCollection);
        }
        
        public void LoadData()
        {
            try
            {
                EndPointsCollection = _dataSource.LoadData();
            }
            catch (Exception)
            {
                EndPointsCollection = new EndPointCollection.EndPointCollection();
                throw;
            }
        }
    }
}
