﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using RAdminLauncherCore.Iterfaces.Configuration;
using RAdminLauncherCore.Iterfaces.DataSource;
using RAdminLauncherCore.Iterfaces.Encryptors;
using RAdminLauncherCore.Iterfaces.EndPointCollection;
using RAdminLauncherCore.Serializers;

namespace RAdminLauncherCore.Implementations.DataSource
{
    public sealed class JsonDataSourceV1 : IDataSource
    {
        private readonly IEncryptor _encryptor;
        private readonly string _jsonFilePath;
        private readonly EndPointListDataSerializer _serializer;

        public JsonDataSourceV1(IConfiguration configuration, IEncryptor encryptor, EndPointListDataSerializer serializer)
        {
            _encryptor = encryptor;
            _jsonFilePath = configuration.JsonPath;
            _serializer = serializer;
        }

        public IEndPointsCollection LoadData()
        {
            if (File.Exists(_jsonFilePath))
            {
                var data = File.ReadAllText(_jsonFilePath);
                var decryptedData = _encryptor.Decrypt(data);
                var endPoints = _serializer.Desserialize(decryptedData);
                var collection = new EndPointCollection.EndPointCollection(endPoints);
                return collection;
            }
            else
            {
                return new EndPointCollection.EndPointCollection();
            }
        }

        public void SaveChanges(IEndPointsCollection collection)
        {
            var pointList = collection.Endpoints.Cast<EndPoint.EndPoint>().ToList();
            var serializedData = _serializer.Serialize(pointList);
            var encryptedData = _encryptor.Encrypt(serializedData);
            File.WriteAllText(_jsonFilePath, encryptedData);
        }
    }
}
