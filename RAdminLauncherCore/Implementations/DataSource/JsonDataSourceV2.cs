﻿using System.IO;
using RAdminLauncherCore.Iterfaces.Configuration;
using RAdminLauncherCore.Iterfaces.DataSource;
using RAdminLauncherCore.Iterfaces.Encryptors;
using RAdminLauncherCore.Iterfaces.EndPointCollection;
using RAdminLauncherCore.Serializers;

namespace RAdminLauncherCore.Implementations.DataSource
{
    public sealed class JsonDataSourceV2 : IDataSource
    {
        private readonly string _jsonFilePath;
        private readonly IEncryptor _encryptor;
        private readonly EndPointCollectionDataSerializer _serializer;

        public JsonDataSourceV2(IConfiguration configuration, IEncryptor encryptor, EndPointCollectionDataSerializer serializer)
        {
            _jsonFilePath = configuration.JsonPath;
            _encryptor = encryptor;
            _serializer = serializer;
        }
        
        public void SaveChanges(IEndPointsCollection collection)
        {
            var serializedData = _serializer.Serialize(collection);
            var encryptedData = _encryptor.Encrypt(serializedData);
            File.WriteAllText(_jsonFilePath, encryptedData);
        }

        public IEndPointsCollection LoadData()
        {
            if (File.Exists(_jsonFilePath))
            {
                var data = File.ReadAllText(_jsonFilePath);
                var decryptedData = _encryptor.Decrypt(data);
                var result = _serializer.Desserialize(decryptedData);
                return result;
            }
            else
            {
                return new EndPointCollection.EndPointCollection();
            }
        }
    }
}
