﻿using System;
using RAdminLauncherCore.Enums;
using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Implementations.Dtos
{
    public class EndPointDto : IEndPointDto
    {
        public string Id { get; }
        public string Name { get; set; }
        public string Ip { get; set; }
        public string Password { get; set; }
        public string User { get; set; }
        public string Port { get; set; }
        public string Address { get; set; }
        public EndPointVersion Version { get; set; }

        public EndPointDto()
        {
            
        }

        public EndPointDto(IEndPoint endPoint)
        {
            Id = endPoint.Id;
            Name = endPoint.Name;
            Ip = endPoint.Ip.ToString();
            Address = endPoint.Address;
            Name = endPoint.Name;
            Password = endPoint.Password;
            Port = endPoint.Port.ToString();
            User = endPoint.User;
            Version = endPoint.Version;
        }
    }
}
