﻿using System;
using System.Net;
using RAdminLauncherCore.Enums;
using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Implementations.EndPoint
{
    public class EndPoint : IEndPoint
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public IPAddress Ip { get; private set; }
        public string Password { get; private set; }
        public string User { get; private set; }
        public int Port { get; private set; }
        public string Address { get; private set; }
        public EndPointVersion Version { get; private set; }

        private EndPoint()
        {
            Id = Guid.NewGuid().ToString();
        }

        public EndPoint(IEndPointDto dto)
        {
            Id = dto.Id ?? Guid.NewGuid().ToString();
            Name = dto.Name;
            Ip = IPAddress.Parse(dto.Ip);
            Password = dto.Password;
            User = dto.User;
            Port = int.Parse(dto.Port);
            Address = dto.Address;
            Version = dto.Version;
        }

        public EndPoint(string name, string ip, string user, string password, int port, EndPointVersion version, string address)
        {
            Id = Guid.NewGuid().ToString();
            Name = name;
            Ip = IPAddress.Parse(ip);
            Password = password;
            User = user;
            Port = port;
            Address = address;
            Version = version;
        }
    }
}
