﻿using RAdminLauncherCore.Iterfaces.EndPointCollection;

namespace RAdminLauncherCore.Iterfaces.DataSource
{
    public interface IDataSourceWriter
    {
        void SaveChanges(IEndPointsCollection collection);
    }
}
