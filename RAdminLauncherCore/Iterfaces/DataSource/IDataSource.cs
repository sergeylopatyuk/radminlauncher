﻿using RAdminLauncherCore.Iterfaces.EndPointCollection;

namespace RAdminLauncherCore.Iterfaces.DataSource
{
    public interface IDataSource :IDataSourceReader, IDataSourceWriter
    {
        
    }
}
