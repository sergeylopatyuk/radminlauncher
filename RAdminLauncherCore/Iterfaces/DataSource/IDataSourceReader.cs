﻿using RAdminLauncherCore.Iterfaces.EndPointCollection;

namespace RAdminLauncherCore.Iterfaces.DataSource
{
    public interface IDataSourceReader
    {
        IEndPointsCollection LoadData();
    }
}
