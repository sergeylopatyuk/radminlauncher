﻿namespace RAdminLauncherCore.Iterfaces.Serializers
{
    public interface IDataSerializer<T>
    {
        T Desserialize(string data);
        string Serialize(T endpointCollection);
    }
}
