﻿using System.Collections.Generic;
using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Iterfaces.EndPointCollection
{
    public interface IEndPointsCollection
    {
        IReadOnlyCollection<IEndPoint> Endpoints { get; }
        IReadOnlyCollection<IEndPoint> GetEndpointsByFilter(string name, string ip, string address);
        void AddOrUpdateEndpoint(IEndPoint endPoint);
        int RemoveEndpoint(string id);
    }
}
