﻿namespace RAdminLauncherCore.Iterfaces.Configuration
{
    public interface IConfiguration
    {
        string RemoteAdminFilePath { get; }
        int RemoteAdminSleepInterval { get; }
        string JsonPath { get; }
        string EncryptionKey { get; }
        string DefaultPort { get; }
    }
}
