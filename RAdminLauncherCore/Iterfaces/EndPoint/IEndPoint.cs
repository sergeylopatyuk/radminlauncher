﻿using System.Net;
using Newtonsoft.Json;
using RAdminLauncherCore.Enums;
using RAdminLauncherCore.Serializers;

namespace RAdminLauncherCore.Iterfaces.EndPoint
{
    public interface IEndPoint
    {
        string Id { get;  }
        string Name { get; }
        [JsonConverter(typeof(IpAddressConverter))]
        IPAddress Ip { get; }
        string Password { get; }
        string User { get; }
        int Port { get; }
        string Address { get; }
        EndPointVersion Version { get; }
    }
}
