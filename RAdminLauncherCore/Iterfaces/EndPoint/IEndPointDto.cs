﻿using RAdminLauncherCore.Enums;

namespace RAdminLauncherCore.Iterfaces.EndPoint
{
    public interface IEndPointDto
    {
        string Id { get; }
        string Name { get; set; }
        string Ip { get; set; }
        string Password { get; set; }
        string User { get; set; }
        string Port { get; set; }
        string Address { get; set; }
        EndPointVersion Version { get; set; }
    }
}
