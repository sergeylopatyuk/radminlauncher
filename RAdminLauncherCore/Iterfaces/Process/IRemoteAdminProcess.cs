﻿using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Iterfaces.Process
{
    public interface IRemoteAdminProcess
    {
        void StartRAdmin(IEndPoint endPoint);
    }
}
