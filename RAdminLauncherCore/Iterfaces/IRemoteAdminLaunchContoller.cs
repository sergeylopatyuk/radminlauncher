﻿using RAdminLauncherCore.Iterfaces.EndPointCollection;
using RAdminLauncherCore.Iterfaces.Process;

namespace RAdminLauncherCore.Iterfaces
{
    public interface IRemoteAdminLaunchContoller
    {
        IRemoteAdminProcess RemoteAdminLauncher { get; }
        IEndPointsCollection EndPointsCollection { get; }
        void SaveChanges();
        void LoadData();
    }
}
