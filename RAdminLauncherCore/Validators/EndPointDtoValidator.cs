﻿using System.Net;
using FluentValidation;
using RAdminLauncherCore.Iterfaces.EndPoint;

namespace RAdminLauncherCore.Validators
{
    public class EndPointDtoValidator : AbstractValidator<IEndPointDto>
    {
        public EndPointDtoValidator()
        {
            RuleFor(x => x.Version).IsInEnum().WithMessage("Версия не корректна");
            RuleFor(x => x.Ip).Must(x => IPAddress.TryParse(x, out var ip)).WithMessage("Некорректный IP адресс");
            RuleFor(x => x.Name).NotEmpty().WithMessage("Название не может быть пустым");
            RuleFor(x => x.Port).Must(x => int.TryParse(x, out var port)).WithMessage("Некорректный номер порта");
            RuleFor(x => x.User).NotEmpty().WithMessage("Логин не может быть пустым");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Пароль не может быть пустым");
        }
    }
}
